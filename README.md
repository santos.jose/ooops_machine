# OoOps_machine

1. Instalación del entorno de Docker. Se recomienda instalar el entorno de Docker
en una máquina GNU/Linux o en un Mac. Se puede utilizar de igual maneral
Windows, pero hay un paso en el despliegue de los contenedores en el que
deberás tener en cuenta este hecho. Se explicará más adelante. Puedes instalar
docker desde aquí: https://www.docker.com/products/docker-desktop o
con:
apt update && apt-get install docker-ce docker-ce-cli containerd.io
Hay que instalar git para descargar

2. Configuración del entorno de Docker y despliegue

	a. Asegúrate con un terminal que Docker está disponible. Por ejemplo,
	puedes ejecutar el comando “service docker status” o “docker imges”.

	b. Desplegar el entorno. El entorno consta de dos contenedores. A continuación
	se mostrará cómo desplegarlos.

3. Contenedor 1: OoOps_machine.

	a. Tienes que solicitar acceso al tutor, ya que el repositorio de código es
	privado.

	b. Ve al siguiente sitio web:
	https://gitlab.com/santos.jose/ooops_machine. Ejecuta la siguiente
	instrucción en un terminal
	“git clone https://gitlab.com/santos.jose/ooops_machine”.

	c. Tendrás una carpeta denominada OoOps_machine. Esta carpeta tendrá
	varios ficheros. El fichero importante es Dockerfile.

	d. Ahora, ejecuta el siguiente comando:
	“docker build . -t tfm:machine1”

	e. Ahora, tenemos la máquina OoOps_machine preparada. Se puede
	comprobar con el comando: “docker images”.

	f. Para lanzar el contenedor ejecutaremos la siguiente instrucción:
	docker run --rm -it -e "IP=YOUR_IP" -p 21:21 -p 22:22 -p 8080:80 -p 10000:10000 tfm:machine1
	IMPORTANTE: Dónde pone YOUR_IP hay que poner la dirección IP de la máquina física.
	Si estás en Mac puedes poner -e “IP=$(ifconfig en0 | grep "inet " | awk -F' ' '{print $2}')”
	Si estás en Linux puedes poner -e “IP=$(ifconfig eth0 | grep "inet " | awk -F' ' '{print
	$2}')
	Si no te funciona bien, pon directamente la IP. Por ejemplo: -e “IP=192.168.0.11”, si tu
	dirección IP es la 192.168.0.11, si no revisa y pon tu dirección IP.

	g. Si quieres parar el contenedor, puedes ejecutar el siguiente comando
	en otro terminal. Primero miramos los contenedores activos y con el
	segundo comando lo paramos.

4. Contenedor 2: Odyssey.

	a. Tienes que solicitar acceso al tutor, ya que el repositorio de código es
	privado.

	b. Ve al siguiente sitio web:
	https://gitlab.com/santos.jose/odyssey. Ejecuta la siguiente
	instrucción en un terminal
	“git clone https://gitlab.com/santos.jose/odyssey.git”

	c. Tendrás una carpeta denominada odyssey. Esta carpeta tendrá varios
	ficheros. El fichero importante es Dockerfile.

	d. Ahora, ejecuta el siguiente comando:
	“docker build . -t tfm:machine2”

	e. Para lanzar el contenedor ejecutamos la siguiente instrucción:
	“docker run --rm -it -p 8080:80 tfm:machine2”
	IMPORTANTE: Si levantas ambos contenedores a la vez, no podrás utilizar el puerto
	8080 de la máquina local (de la máquina física), por lo que utiliza otro puerto que tu
	quieras.

5. Se recomienda que utilicéis una máquina Kali Linux con diferentes herramientas
de seguridad, aunque esto es solo una recomendación. 

6. Una vez configurado el entorno se deberán ir completando los siguientes hitos.
Hay que tener en cuenta que se puede hacer primero una máquina y luego
otra. No hay dependencia entre máquinas, ni hay relación. Son dos máquinas
sin relación.

7. El primer hito es llevar a cabo una enumeración de servicios, puertos y
software que se ejecutan en ambos contenedores. Tened en cuenta que una
vez levantados los contenedores no se puede acceder a su interior. SOLO vale
todo lo que se haga de forma remota a través de herramientas de pentesting.
Además, como complemento, se puede utilizar escáneres de análisis de
vulnerabilidades para buscar o detectar vulnerabilidades conocidas en el sistema.

8. El segundo hito es lograr la explotación de una vulnerabilidad o el aprovechamiento
de alguna debilidad en la máquina OoOps_machine. Se recomienda
hacer una evaluación de los servicios. En este hito se tiene que conseguir
el primer flag.txt. Se debe explicar con todo detalle: el camino hecho para
lograr el flag, cómo se solventa, a qué software afecta. PISTA: Se accede con
un usuario, pero el flag pertenece a otro.

9. El tercer hito es lograr la escalada de privilegios en la máquina
OoOps_machine. En el hito anterior se consiguió el primer flag.txt, y con esta
escalada de privilegios tienes que llegar a ser el usuario root en la máquina.
Cuando seas el usuario root tienes que lograr el segundo flag.txt. Documenta
todo el proceso paso a paso y explica cómo has logrado dicha escalada de privilegios
en la máquina OoOps_machine.

10. El cuarto hito  es lograr explotar alguna vulnerabilidad en la máquina
Odyssey. Esta máquina solo tiene un flag.txt (que será el tercero que consigas),
el cual debes encontrar cuando logres acceso a la máquina. Explica con
detalle todo el proceso.odyssey machine
